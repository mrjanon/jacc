package org.janon.jacc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JaccApplication {

	public static void main(String[] args) {
		SpringApplication.run(JaccApplication.class, args);
	}

}
